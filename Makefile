SHELL := /bin/bash

.PHONY: fmt
fmt:
	go fmt ./...
.PHONY: vet
vet:
	go vet ./...

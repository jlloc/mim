package mim

type Client struct {
	Config       *ConfigManager
	Instances    *InstanceManager
	UserAccounts *UserManager
	initialized  bool
}

func NewClient(base string) *Client {
	return &Client{
		Config:       NewConfigManager(base),
		Instances:    NewInstanceManager(base),
		UserAccounts: NewUserManager(base),
	}
}

func (c *Client) Init() error {
	cg := ConnectionGroup{
		c.Config,
		c.Instances,
		c.UserAccounts,
	}

	if err := cg.InitAll(); err != nil {
		return err
	}

	c.initialized = true
	return nil
}

func (c *Client) Close() error {
	if !c.initialized {
		return nil
	}

	cg := ConnectionGroup{
		c.Config,
		c.UserAccounts,
		c.Instances,
	}

	return cg.CloseAll()
}

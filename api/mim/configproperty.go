package mim

type ConfigProperty struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

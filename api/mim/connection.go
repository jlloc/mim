package mim

import (
	"sync"
)

type Connection interface {
	Init() error
	Close() error
}

type ConnectionGroup []Connection

func (cg *ConnectionGroup) InitAll() error {
	var err error
	var once sync.Once
	var wg sync.WaitGroup
	wg.Add(len(*cg))

	for _, c := range *cg {
		go func(con Connection) {
			defer wg.Done()

			if conErr := con.Init(); conErr != nil {
				once.Do(func() { err = conErr })
			}
		}(c)
	}

	wg.Wait()
	return err
}

func (cg *ConnectionGroup) CloseAll() error {
	var err error
	var once sync.Once
	var wg sync.WaitGroup
	wg.Add(len(*cg))

	for _, c := range *cg {
		go func(con Connection) {
			defer wg.Done()

			if conErr := con.Close(); conErr != nil {
				once.Do(func() { err = conErr })
			}
		}(c)
	}

	wg.Wait()
	return err
}

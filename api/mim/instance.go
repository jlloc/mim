package mim

import (
	"gitlab.com/jlocash/mim/api/minecraft"
)

type Instance struct {
	minecraft.Version  `json:"version"`
	Name               string `json:"name"`
	Configured         bool   `json:"configured"`
	BaseDirectory      string `json:"baseDirectory"`
	AssetsDirectory    string `json:"assetsDirectory"`
	LibrariesDirectory string `json:"librariesDirectory"`
}

func (i *Instance) Configure() error {
	if err := i.UpdateMainJAR(); err != nil {
		return err
	}

	if err := i.UpdateAssets(); err != nil {
		return err
	}

	if err := i.UpdateLibraries(); err != nil {
		return err
	}

	return nil
}

func (i *Instance) UpdateMainJAR() error {
	return nil
}

func (i *Instance) UpdateAssets() error {
	return nil
}

func (i *Instance) UpdateLibraries() error {
	return nil
}

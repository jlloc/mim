package mim

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gitlab.com/jlocash/mim/api/minecraft"
)

type InstanceManager struct {
	instances          map[string]*Instance
	LibrariesDirectory string
	AssetsDirectory    string
	BaseDirectory      string
	initialized        bool
}

func NewInstanceManager(base string) *InstanceManager {
	return &InstanceManager{
		instances:          make(map[string]*Instance),
		BaseDirectory:      path.Join(base, "instances"),
		AssetsDirectory:    path.Join(base, "assets"),
		LibrariesDirectory: path.Join(base, "libraries"),
	}
}

func (im *InstanceManager) Add(version minecraft.Version, name string) error {
	if _, ok := im.instances[name]; ok {
		return fmt.Errorf("Instance already exists: %s", name)
	}

	instance := &Instance{
		Name:               name,
		Version:            version,
		BaseDirectory:      path.Join(im.BaseDirectory, name),
		AssetsDirectory:    im.AssetsDirectory,
		LibrariesDirectory: im.LibrariesDirectory,
	}

	im.instances[name] = instance
	return nil
}

func (im *InstanceManager) Get(name string) *Instance {
	instance, exists := im.instances[name]
	if !exists {
		return nil
	}

	return instance
}

func (im *InstanceManager) Init() error {
	config := path.Join(im.BaseDirectory, "instances.json")
	if _, err := os.Stat(config); os.IsNotExist(err) {
		return nil
	}

	bytes, err := ioutil.ReadFile(config)
	if err != nil {
		return err
	}

	if err = json.Unmarshal(bytes, &im.instances); err != nil {
		return err
	}

	im.initialized = true
	return nil
}

func (im *InstanceManager) Close() error {
	if !im.initialized {
		return nil
	}

	bytes, err := json.MarshalIndent(im.instances, "", "    ")
	if err != nil {
		return err
	}

	if _, err = os.Stat(im.BaseDirectory); os.IsNotExist(err) {
		if err = os.MkdirAll(im.BaseDirectory, os.ModePerm); err != nil {
			return err
		}
	}

	config := path.Join(im.BaseDirectory, "instances.json")
	return ioutil.WriteFile(config, bytes, 0644)
}

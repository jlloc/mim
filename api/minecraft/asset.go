package minecraft

import (
	"fmt"
	"log"
	"path"
)

const ASSET_DOWNLOAD_URL = "https://resources.download.minecraft.net"

type Asset struct {
	Hash string `json:"hash"`
	Size int    `json:"size"`
}

func (a *Asset) Update(dir string) error {
	if len(a.Hash) != 40 {
		return fmt.Errorf("Asset download failed (bad hash detected).")
	}

	url := fmt.Sprintf("%s/%s/%s", ASSET_DOWNLOAD_URL, a.Hash[:2], a.Hash)
	filepath := path.Join(a.Hash[:2], a.Hash)

	f := FileDownloadInfo{
		DownloadInfo: DownloadInfo{
			SHA1: a.Hash,
			Size: a.Size,
			URL:  url,
		},
		Path: filepath,
	}

	if exists := f.Check(dir); !exists {
		return f.Download(dir)
	}

	if err := f.Verify(dir); err != nil {
		log.Println(err.Error())
		return f.Download(dir)
	}

	return nil
}

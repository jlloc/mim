package minecraft

type AssetIndexInfo struct {
	DownloadInfo
	ID        string `json:"id"`
	TotalSize int    `json:"totalSize"`
}

type AssetIndex struct {
	Objects map[string]Asset `json:"objects"`
}

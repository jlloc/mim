package minecraft

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
)

type DownloadInfo struct {
	SHA1 string `json:"sha1"`
	Size int    `json:"size"`
	URL  string `json:"url"`
}

func (d *DownloadInfo) Download() ([]byte, error) {
	resp, err := http.Get(d.URL)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	hash := sha1.Sum(bytes)
	if hex.EncodeToString(hash[:]) != d.SHA1 {
		return nil, fmt.Errorf("File verification failed (invalid checksum).")
	}

	return bytes, nil
}

type FileDownloadInfo struct {
	DownloadInfo
	Path string `json:"path"`
}

func (f *FileDownloadInfo) Check(dir string) bool {
	_, err := os.Stat(path.Join(dir, f.Path))
	return !os.IsNotExist(err)
}

func (f *FileDownloadInfo) Download(dir string) error {
	log.Println("Acquiring file")

	bytes, err := f.DownloadInfo.Download()
	if err != nil {
		return err
	}

	filepath := path.Join(dir, f.Path)
	if err = os.MkdirAll(path.Dir(filepath), os.ModePerm); err != nil {
		return err
	}

	return ioutil.WriteFile(filepath, bytes, 0644)
}

func (f *FileDownloadInfo) Verify(dir string) error {
	log.Println("Verifying file checksum")

	filepath := path.Join(dir, f.Path)
	bytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}

	hash := sha1.Sum(bytes)
	if hex.EncodeToString(hash[:]) != f.SHA1 {
		return fmt.Errorf("File verification failed (invalid checksum).")
	}

	return nil
}

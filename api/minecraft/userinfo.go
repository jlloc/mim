package minecraft

type userInfoProperty struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type UserInfo struct {
	ID                string             `json:"id"`
	Email             string             `json:"email"`
	Username          string             `json:"username"`
	RegisterIP        string             `json:"registerIp"`
	MigratedFrom      string             `json:"migratedFrom"`
	MigratedAt        int                `json:"migratedAt"`
	RegisteredAt      int                `json:"registeredAt"`
	PasswordChangedAt int                `json:"passwordChangedAt"`
	DateOfBirth       int                `json:"dateOfBirth"`
	Suspended         bool               `json:"suspended"`
	Blocked           bool               `json:"blocked"`
	Secured           bool               `json:"secured"`
	Migrated          bool               `json:"migrated"`
	EmailVerified     bool               `json:"emailVerified"`
	LegacyUser        bool               `json:"legacyUser"`
	VerifiedByParent  bool               `json:"verifiedByParent"`
	Properties        []userInfoProperty `json:"properties"`
}

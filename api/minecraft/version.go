package minecraft

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"path"
)

type Version struct {
	// Logging                `json:"logging"`
	AssetIndexInfo         AssetIndexInfo          `json:"assetIndex"`
	Arguments              FileDownloadInfo        `json:"arguments"`
	AssetsVersion          string                  `json:"assets"`
	Downloads              map[string]DownloadInfo `json:"downloads"`
	ID                     string                  `json:"id"`
	Libraries              []Library               `json:"libraries"`
	MainClass              string                  `json:"mainClass"`
	MinimumLauncherVersion int                     `json:"minimumLauncherVersion"`
	ReleaseTime            string                  `json:"releaseTime"`
	Time                   string                  `json:"time"`
	Type                   string                  `json:"type"`
}

func (v *Version) Install(dir string) error {
	index := FileDownloadInfo{
		DownloadInfo: v.AssetIndexInfo.DownloadInfo,
		Path:         path.Join("indexes", v.AssetIndexInfo.ID+".json"),
	}

	if err := index.Download(path.Join(dir, "assets")); err != nil {
		return err
	}

	bytes, err := ioutil.ReadFile(path.Join(dir, "assets", "indexes", index.Path))
	if err != nil {
		return err
	}

	var assets AssetIndex
	if err := json.Unmarshal(bytes, &assets); err != nil {
		return err
	}

	for name, asset := range assets.Objects {
		log.Printf("Updating Asset: %s\n", name)

		err := asset.Update(path.Join(dir, "assets", "objects"))
		if err != nil {
			return err
		}
	}

	for _, lib := range v.Libraries {
		log.Printf("Updating library: %s\n", lib.Name)

		err := lib.Update(path.Join(dir, "libraries"))
		if err != nil {
			return err
		}
	}

	return nil
}

package minecraft

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type VersionInfo struct {
	ID          string `json:"id"`
	Type        string `json:"type"`
	URL         string `json:"url"`
	Time        string `json:"time"`
	ReleaseTime string `json:"releaseTime"`
}

func (vi *VersionInfo) GetVersion() (Version, error) {
	var v Version

	resp, err := http.Get(vi.URL)
	if err != nil {
		return v, err
	}

	defer resp.Body.Close()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return v, err
	}

	err = json.Unmarshal(bytes, &v)
	return v, err
}

package main

import (
	cmd "gitlab.com/jlocash/mim/cmd/mim/subcmd"
)

func main() {
	cmd.Execute()
}
